public class MaxDelSum2
{
	private static int count = 0;
	
	public static void main(String[] args)
	{
		int[] data = {31,-41,59,26,-53,58,97,-93,-23,84};
		System.out.println("data = {31,-41,59,26,-53,58,97,-93,-23,84}");
		System.out.println();
		System.out.println("Algorithm 4 - A Scanning Algorithm: "+scanningAlgorithm(data));		
		System.out.println();
		
		// Two dimensional:
		int[][] data2 = {
			{1,2,3,4,5,1,2,3,4,5},
			{10,20,30,40,50,1,2,3,4,5},
			{100,200,-300,400,500,1,2,3,4,5},
			{1000,2000,3000,4000,5000,1,2,3,4,5},
			{10000,20000,30000,400000,50000,1,2,3,4,5},
			{1,2,3,4,5,1,2,3,4,5},
			{1,2,3,4,5,1,2,3,4,5},
			{1,2,3,4,5,1,2,3,4,5},
			{1,2,3,4,5,1,2,3,4,5},
			{1,2,3,4,5,1,2,3,4,5},
			};
		long tmp = System.currentTimeMillis();
        int vn = 700;
		int[][] data3 = new int[vn][vn];
		// System.out.println("data2 = "); print2DimArray(data2);
		// System.out.println();
		System.out.println("Algorithm in Two Dimensions - Our approach: " +""+twoDimAlgorithm(data3, data3.length));
		System.out.println("AddCounter: " + count+" after "+(System.currentTimeMillis() - tmp) + " millis");
	}
	
	/**
	 * Max Delsum in 2 dimensions square array:
	 * Making every possible Sum on one dimension first
	 * Probably O(n^3) complexity
	 * @param n The length of the sides in the array
	 * @precondition x is a two dimensional array
	 */
	private static int twoDimAlgorithm(int[][] x, int n)
	{
		assert x[0].length == x.length: ""+x+" is not quadratic";
		
		int[][] cummulativeArray = new int[n+1][n]; // allow room for a row with zeros
		
		//print2DimArray(cummulativeArray);
		// set cummulativeArray up to hold the cummulative sum of the earlier values
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
			{
				cummulativeArray[i+1][j] = cummulativeArray[i][j] + x[i][j];
				count++;
			}
		}
		//print2DimArray(cummulativeArray);
		
		int maxSoFar = 0;
		for (int i = 0; i < n; i++)
		{
			int[] sum = new int[n];
			for (int j = i; j < n; j++)
			{
				for (int k = 0; k < n; k++)
				{
					sum[k] = cummulativeArray[j+1][k] - cummulativeArray[i][k];
					count++;
				}
				maxSoFar = Math.max(maxSoFar, scanningAlgorithm(sum));
			}
		}
		return maxSoFar;
	}

	/*
	 * Algorithm 4 - The Best Algorithm
	 * A Scanning Algorithm in 1 dimension
	 * @param x The data array
	 * @invariant maxEndingHere and maxSoFar are accurate for x[0..i-1]
	 */
	private static int scanningAlgorithm(int[] x)
	{
		int maxSoFar = 0, maxEndingHere = 0;
		
		for (int i = 0; i < x.length; i++)
		{
			count++;
			maxEndingHere = Math.max(maxEndingHere + x[i], 0);
			maxSoFar = Math.max(maxSoFar, maxEndingHere);
		}
		return maxSoFar;
	}
	
	private static void print2DimArray(int[][] array)
	{
		for(int i=0; i<array.length; i++) { 
			for(int j=0; j<array[0].length; j++) 
				System.out.print("[" + array[i][j] + "]");
			System.out.println();
		}
	}
}
