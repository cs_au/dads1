// NxM Matrix Data
// HELE TRE DIMENSIONER OG CORNFLAKES

#define N 2
#define M 2
#define O 2
#define DATA_SET {{{1,2},{3,4}}, \
                 {{5,6},{7,8}}}

#include <stdio.h>

// Prototypes!
unsigned int MAX(int x, int y);
unsigned int scanningAlgorithm1D(int input[N]);
unsigned int scanningAlgorithm2D(int input[M][N]);
unsigned int scanningAlgorithm3D(int input[O][M][N]);

// Worst case O(n)
unsigned int scanningAlgorithm1D(int input[N])
{
    unsigned int max_so_far = 0, max_ending_here = 0;

    unsigned int x;    
    for(x=0; x<N; x++)
    {
        max_ending_here = MAX(max_ending_here + input[x], 0);
        max_so_far = MAX(max_ending_here, max_so_far);
    }
    return max_so_far;
}

// Worst case O(n^3)
//      - O(n^2) for all possible row combination. 
//      - O(n)   for the scanningAlgorithm1D.
unsigned int scanningAlgorithm2D(int input[M][N])
{
    int buffer[N];
    int current_val = -1;
    int max_val = -1;

    unsigned int i;
    for(i=0; i<M; i++)
    {
        // Clear buffer
        unsigned int k;
        for(k=0; k<N; k++)
            buffer[k] = 0;

        // Actual processing
        unsigned int j;
        for(j=i; j<M; j++)
        {
            // Get all permutation of input
            unsigned int k;
            for(k=0; k<N; k++)
                buffer[k] += input[j][k];
           
            /* 
            printf("\t");
            for(k=0; k<N; k++)
                printf("[%d]", buffer[k]);
            printf("\n");
            */

            //Check if the found permutation is the best so far
            current_val = scanningAlgorithm1D(buffer);
            max_val = MAX(current_val, max_val);
        }
    }
    return max_val;
}

// Worst case O(n^5)
//      - O(n^2) for all possible flat combination. 
//      - O(n^3) for the scanningAlgorithm2D.
unsigned int scanningAlgorithm3D(int input[O][M][N])
{
    int buffer[M][N];
    int current_val = -1;
    int max_val = -1;

    unsigned int i;
    for(i=0; i<M; i++)
    {
        // Clear buffer
        unsigned int m;
        unsigned int n;
        for(m=0; m<M; m++)
            for(n=0; n<N; n++)
                buffer[m][n] = 0;

        // Actual processing
        unsigned int j;
        for(j=i; j<M; j++)
        {
            // Get all permutation of input
            unsigned int m;
            unsigned int z;
            for(m=0; m<N; m++)    
                for(z=0; z<O; z++)
                    buffer[z][m] += input[z][j][m];
            /*
            for(m=0; m<N; m++)
            {
                printf("{");
                for(z=0; z<O; z++)
                {
                    printf("[%d]", buffer[z][m]);
                }
                printf("}");
            }
            printf("\n");
            */

            //Check if the found permutation is the best so far
            current_val = scanningAlgorithm2D(buffer);
            max_val = MAX(current_val, max_val);
        }
    }
    return max_val;
}

unsigned int MAX(int x, int y)
{
    if(x>y)
        return x;
    else
        return y;
}

int main()
{
    int input[O][M][N] = DATA_SET;
    
    /*
    for(int x=0; x<O; x++)
    {
        printf("(");
        for(int y=0; y<M; y++)
        {
            printf("{");
            for(int z=0; z<N; z++)
                printf("[%d]", input[x][y][z]);
            printf("}");
        }
        printf(")");
    }
    */

    printf("Algorithm in Three Dimensions: %d\n",
            scanningAlgorithm3D(input));
    return 0;
}

