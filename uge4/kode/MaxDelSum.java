public class MaxDelSum
{
        public static void main(String[] args)
        {
                int[] data = {31,-41,59,26,-53,58,97,-93,-23,84};
                System.out.println("data = {31,-41,59,26,-53,58,97,-93,-23,84}");
                System.out.println();
                System.out.println("Algorithm 1 - A Slow Algorithm: "+slowAlgorithm(data));
                System.out.println("Algorithm 2A - A Two Quadratic AlgorithmA: "+twoQuadraticAlgorithmA(data));
                System.out.println("Algorithm 2B - A Two Quadratic AlgorithmB: " +""+twoQuadraticAlgorithmB(data));
                System.out.println("Algorithm 3 - A Divide & Conquer Algorithm: " +"?"+divConquerAlgorithm(data));
                System.out.println("Algorithm 4 - A Scanning Algorithm: "+scanningAlgorithm(data));             
                System.out.println();
                
                // Two dimensional:
                int[][] data2 = {
                        {1,2,3},//,4},
                        {10,20,30},//,40},
                        {100,200,-300},//,400},
                        // {1000,2000,3000,4000}
                        };
                System.out.println("data2 = "); print2DimArray(data2);
                System.out.println();
                System.out.println("Algorithm in Two Dimensions - Our approach: " +""+twoDimAlgorithm(data2, data2.length));
        }
        
        /**
         * Max Delsum in 2 dimensions square array:
         * Making every possible Sum on one dimension first
         * Probably O(n^3) complexity
         * @param n The length of the sides in the array
         * @precondition x is a two dimensional array
         */
        private static int twoDimAlgorithm(int[][] x, int n)
        {
                assert x[0].length == x.length: ""+x+" is not quadratic";
                
                int[][] cummulativeArray = new int[n+1][n]; // allow room for a row with zeros
                
                //print2DimArray(cummulativeArray);
                // set cummulativeArray up to hold the cummulative sum of the earlier values
                for (int i = 0; i < n; i++)
                {
                        for (int j = 0; j < n; j++)
                        {
                                cummulativeArray[i+1][j] = cummulativeArray[i][j] + x[i][j];
                        }
                }
                //print2DimArray(cummulativeArray);
                
                int maxSoFar = 0;
                for (int i = 0; i < n; i++)
                {
                        int[] sum = new int[n];
                        for (int j = i; j < n; j++)
                        {
                                for (int k = 0; k < n; k++)
                                {
                                        sum[k] = cummulativeArray[j+1][k] - cummulativeArray[i][k];
                                }
                                maxSoFar = Math.max(maxSoFar, scanningAlgorithm(sum));
                        }
                }
                return maxSoFar;
        }
        
        private static void print2DimArray(int[][] array)
        {
                for(int i=0; i<array.length; i++) { 
                        for(int j=0; j<array[0].length; j++) 
                                System.out.print("[" + array[i][j] + "]");
                        System.out.println();
                }
        }
        
        /*
         * Algorithm 1
         * A Slow Algorithm
         * @param x The data array
         */
        private static int slowAlgorithm(int[] x)
        {
                int maxSoFar = 0;
                
                for (int i = 0; i < x.length; i++)
                {
                        for (int j = i; j < x.length; j++)
                        {
                                int sum = 0;
                                for (int k = i; k <= j; k++)
                                {
                                        sum+= x[k];
                                }
                                /* sum is sum of x[i..j] */
                                maxSoFar = Math.max(sum, maxSoFar);
                        }
                }
                return maxSoFar;
        }
        /*
         * Algorithm 2
         * Two Quadratic Algorithm in 1 dimension
         * @param x The data array
         */
        private static int twoQuadraticAlgorithmA(int[] x)
        {
                int maxSoFar = 0;
                
                for (int i = 0; i < x.length; i++)
                {
                        int sum = 0;
                        for (int j = i; j < x.length; j++)
                        {
                                sum+= x[j];
                                /* sum is sum of x[i..j] */
                                maxSoFar = Math.max(sum, maxSoFar);
                        }
                }
                return maxSoFar;
        }
        
        private static int twoQuadraticAlgorithmB(int[] x)
        {
                int n = x.length;
                int[] cummulativeArray = new int[n+1]; //To allow for value 0 at index 0 (instead of index -1)
                cummulativeArray[0] = 0;
                
                for (int i = 0; i < n; i++)
                {
                        cummulativeArray[i + 1] = cummulativeArray[i] + x[i];
                }
                
                int maxSoFar = 0;
                for (int i = 0; i < n; i++)
                {
                        int sum = 0;
                        for (int j = i; j < n; j++)
                        {
                                sum = cummulativeArray[j+1] - cummulativeArray[i];
                                maxSoFar = Math.max(maxSoFar, sum);
                        }
                }
                return maxSoFar;
        }
        
        /*
         * Algorithm 3
         * Divide and Conquer-Algorithm in 1 dimension
         */
        private static int divConquerAlgorithm(int[] x)
        {
                return maxSum3( 0, x.length-1, x);
        }
        
        /*
         * @ param l, u Is the lower bound and upper bound to search in
         * NOT FINISHED, THERE IS SOME BUG IN THIS CODE
         */
        private static int maxSum3(int l, int u, int[] x)
        {
                if (l>u) return 0; //zero elements
                if (l==u) return Math.max(0, x[l]); // one element
                
                // Middle
                int m = (l+u) / 2;
                
                // find max crossing to the left
                int lmax = 0, sum = 0;
                for (int i = m; i >= l; i--)
                {
                        sum+= x[i];
                        lmax = Math.max(lmax, sum);
                }
                
                // find max crossing to the right
                int rmax = 0; sum = 0;
                for (int i = m; i <= u; i++)
                {
                        sum+= x[i];
                        lmax = Math.max(rmax, sum);
                }
                
                return Math.max(
                                  lmax+rmax,
                                  Math.max(maxSum3(m+1,u,x), maxSum3(l,m,x))
                           );
        }
        
        /*
         * Algorithm 4 - The Best Algorithm
         * A Scanning Algorithm in 1 dimension
         * @param x The data array
         * @invariant maxEndingHere and maxSoFar are accurate for x[0..i-1]
         */
        private static int scanningAlgorithm(int[] x)
        {
                int maxSoFar = 0, maxEndingHere = 0;
                
                for (int i = 0; i < x.length; i++)
                {
                        maxEndingHere = Math.max(maxEndingHere + x[i], 0);
                        maxSoFar = Math.max(maxSoFar, maxEndingHere);
                }
                return maxSoFar;
        }
}

