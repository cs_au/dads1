// NxM Matrix Data
#define N 3
#define M 3
#define DATA_SET {{-800,20,30},    \
                  {1,2,3},         \
                  {1,200,300}} //=555

#include <stdio.h>

// Prototypes!
unsigned int MAX(int x, int y);
unsigned int scanningAlgorithm(int *input);
unsigned int scanningAlgorithm2D(int input[M][N]);

// Simple scanning algorithm, for finding greatest subvector, within vector.
unsigned int scanningAlgorithm(int *input)
{
    unsigned int max_so_far = 0, max_ending_here = 0;

    unsigned int x;    
    for(x=0; x<N; x++)
    {
        max_ending_here = MAX(max_ending_here + input[x], 0);
        max_so_far = MAX(max_ending_here, max_so_far);
    }
    return max_so_far;
}

// Worst case O(n^3)
//      - O(n^2) for all possible row combination. 
//      - O(n)   for the scanningAlgorithm.
// Teoretic bestcase O(1) - All entries are positive, and we know this beforehand.
unsigned int scanningAlgorithm2D(int input[M][N])
{
    int buffer[N];
    int current_val = -1;
    int max_val = -1;

    unsigned int i;
    for(i=0; i<M; i++)
    {
        // Clear buffer
        unsigned int k;
        for(k=0; k<N; k++)
            buffer[k] = 0;

        // Actual processing
        unsigned int j;
        for(j=i; j<M; j++)
        {
            // Get all permutation of input
            unsigned int k;
            for(k=0; k<N; k++)
                buffer[k] += input[j][k];
            
            //Check if the found permutation is the best so far
            current_val = scanningAlgorithm(buffer);
            max_val = MAX(current_val, max_val);
        }
    }
    return max_val;
}

unsigned int MAX(int x, int y)
{
    if(x>y)
        return x;
    else
        return y;
}

int main()
{
    int input[M][N] = DATA_SET;
    printf("Algorithm in Two Dimensions: %d\n",
            scanningAlgorithm2D(input));
    return 0;
}

