#include <iostream>
#include <cstdlib>
using namespace std;

const static int N = 3;
const static int M = 3;
#define INFITY 1000
#define RANDOM_INPUT

void printValue(int val)
{
    if(val == INFITY)
        cout << "@";
    else
        cout << val;
}

void printTable(int Array[N][M], int i=N, int j=M)
{
    cout << "{" << endl;
    for(int x=0; x<i; x++)
    {
        cout << "\t[";
        for(int y=0; y<j-1; y++)
        {
            printValue(Array[x][y]);
            cout << ", ";
        }
        printValue(Array[x][j-1]);
        cout << "]" << endl;
    }
    cout << "}" << endl;
}

// Sorts in O(n) time.
void sort(int *Array, int length)
{
    for(int x=0; x<length-1; x++)
    {
        if(Array[x] > Array[x+1])
        {
            swap(Array[x], Array[x+1]);
        }
    }
}

void transpose(int Array[N][M])
{
    for (unsigned int i=1; i<M; i++)
        for (unsigned int j=0; j<i; j++)
            swap(Array[i][j], Array[j][i]);
}

int compare(const void *a, const void *b)
{
    return ( *(int*)a - *(int*)b );
}

void sortTable(int Array[N][M])
{
    for(int y=0; y<M; y++)
        qsort(Array[y], M, sizeof(int), compare);
    transpose(Array);
    for(int x=0; x<N; x++)
        qsort(Array[x], N, sizeof(int), compare);
    transpose(Array);
}

void Tableauify(int Array[N][M])
{
    for(int y=0; y<M; y++)
    {
        printTable(Array);
        sort(Array[y], M);
    }
    transpose(Array);
    for(int x=0; x<N; x++)
    {
        printTable(Array);
        sort(Array[x], N);
    }
    transpose(Array);
    printTable(Array);
}

void Tables(int Array[N][M], int i, int j)
{
    //FUCK STATEMENT 
    if((i+1 == N) && (j+1 == M))
        return;
    else if(((i+1) == N) && (j+1 != M))
    {
        printTable(Array);
        swap(Array[i][j+1], Array[i][j]);
        Tables(Array, i, j+1);
    }
    else if(((i+1) != N) && (j+1 == M))
    {
        printTable(Array);
        swap(Array[i+1][j], Array[i][j]); 
        Tables(Array, i+1, j);
    }
    else
    {
        printTable(Array);
        if(left < right)
        {
            swap(Array[i+1][j], Array[i][j]); 
            Tables(Array, i+1, j);
        }
        else
        {
            swap(Array[i][j+1], Array[i][j]);
            Tables(Array, i, j+1);
        }
    }
}

void Sverres(int Array[N][M])
{
    Tables(Array, 0, 0);
}

int EXTRACT_MIN(int Array[N][M], void (*func)(int Array[N][M]))
{
    printTable(Array);
    
    int min = Array[0][0];
    Array[0][0] = INFITY;

    func(Array);

    printTable(Array);
    
    return min;
}

void CHECK(int Array[N][M])
{
    for(int i=0; i<N-1; i++)
    {
        for(int j=0; j<M-1; j++)
        {
            if((Array[i][j] > Array[i][j+1]) ||
               (Array[i][j] > Array[i+1][j]))
            {
                cout << "FUCK YOUZ ERRORZZ HAZZ HAPPENZ!" << endl;
                printTable(Array);
                system("PAUSE");
            }
        }
    }
}

int main()
{
    while(1)
    {
        #ifdef RANDOM_INPUT
            int Array[N][M];

            // Clean up
            for(int x=0; x<N; x++)
                for(int y=0; y<M; y++)
                    Array[x][y] = (rand()%INFITY);
        #else
            int Array[N][M] =
            {
                {3,   4,    5},
                {8,   9,    INFITY},
                {10,  INFITY, INFITY}
            };
        #endif

        cout << "START!" << endl;
        //sortTable(Array);
        //EXTRACT_MIN(Array, Tableauify);
        EXTRACT_MIN(Array, Sverres);
        CHECK(Array);
    }
    return 0;
}
